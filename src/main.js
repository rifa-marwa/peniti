import Vue from 'vue';
import router from './router';
import store from './store';
import i18n from './plugins/vue-i18n';
import './plugins';

/**
 * Styles
 */
import '@/assets/styles/tailwind.css';
import '@/assets/styles/custom.sass';

/**
 * Base Components
 */
import App from './App.vue';
import EmptyLayout from '@/layouts/empty';
import DefaultLayout from '@/layouts/default';
import FormGroup from '@/components/Base/FormGroup';

/**
 * Components Auto Register
 */
Vue.component('emptyLayout', EmptyLayout);
Vue.component('defaultLayout', DefaultLayout);
Vue.component('FormGroup', FormGroup);

/**
 * Instance
 */

Vue.config.productionTip = false;

new Vue({
  i18n,
  store,
  router,
  render: h => h(App)
}).$mount('#app');
