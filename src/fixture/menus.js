/* ========================================================================================
 * File Name : menus.js
 * Description : menu items list. add/remove menu items here
 *
 * Structure :
 *  [ROLE] = {
 *   url => router path
 *   i18n => internationalization (file: locales/en.json)
 * }
 *
 * NOTE:
 * menu items list not support to sub menu, if there is adjustment about sub menu custom it
 * ======================================================================================== */

export default {
  DEFAULT: [
    {
      url: 'Dashboard',
      i18n: 'content.dashboard.title'
    },
    {
      i18n: 'content.user.title',
      children: [
        {
          url: 'UserIndex',
          i18n: 'content.user.user_management_title'
        },
        {
          url: 'UserPerformanceIndex',
          i18n: 'content.user.user_performance_title'
        }
      ]
    },
    {
      i18n: 'content.checklist.top_level',
      children: [
        {
          url: 'ChecklistIndex',
          i18n: 'content.checklist.title'
        },
        {
          url: 'ReportIndex',
          i18n: 'content.report.title'
        }
      ]
    },
    {
      url: 'SensorIndex',
      i18n: 'content.sensor.title'
    }
  ]
};
