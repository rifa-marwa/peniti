import Wrapper from '@/components/Base/Wrapper';

export default [
  {
    path: '/auth',
    component: Wrapper,
    children: [
      {
        path: '',
        name: 'Auth',
        component: () => import(/* webpackChunkName: "auth-login" */ '@/pages/auth')
      }
    ]
  }
];
