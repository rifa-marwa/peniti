export default [
  {
    path: '*',
    component: () => import(/* webpackChunkName: "not-found" */ '@/pages/notfound')
  }
];
