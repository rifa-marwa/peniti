import InformationWrapper from '../pages/information/index.vue';

export default [
  {
    path: '/informations',
    component: InformationWrapper,
    children: [
      {
        path: '',
        name: 'InformationCommon',
        component: () => import(/* webpackChunkName: "information-common" */ '@/pages/information')
      }
    ]
  }
];
