import Vue from 'vue';
import Router from 'vue-router';
import Store from '../store';

import DashboardWrapper from '@/components/Base/DashboardWrapper';

// Routes
import authRoutes from './authRoutes';
import informationRoutes from './informationRoutes';
import utilRoutes from './utilRoutes';

Vue.use(Router);

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '',
      redirect: () => {
        if (Store.getters['Auth/isAuthenticated']) {
          return { name: 'Dashboard' };
        }
        return { name: 'Auth' };
      }
    },
    {
      path: '/dashboard',
      component: DashboardWrapper,
      children: [
        {
          path: '',
          name: 'Dashboard',
          meta: { requiresAuth: true, layout: 'default' },
          component: () => import(/* webpackChunkName: "dashboard" */ '@/pages/dashboard')
        }
      ]
    },
    ...authRoutes,
    ...informationRoutes,
    ...utilRoutes
  ]
});

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (!Store.getters['Auth/isAuthenticated']) {
      next({ name: 'Auth', replace: true });
    } else {
      next();
    }
  } else {
    next();
  }
});

export default router;
