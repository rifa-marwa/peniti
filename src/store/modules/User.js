const namespaced = true;

const state = {
  isUserLoading: false,
  userChartData: []
};

const getters = {};

const mutations = {
  SET_USER_LOADING: state => (state.isUserLoading = true),
  SET_USER_CHART_DATA: (state, payload) => {
    state.userChartData = payload;
    state.isServiceLoading = false;
  }
};

const actions = {
  getUserChartData: async ({ commit }) => {
    commit('SET_USER_LOADING');

    try {
      const { data } = await window.axios.get('/users/chart-dashboard');

      let userChartValues = [];
      data.results.forEach(user => {
        userChartValues.push(user.value);
      });

      commit('SET_USER_CHART_DATA', userChartValues);

      return Promise.resolve(userChartValues);
    } catch (err) {
      return Promise.reject(err);
    }
  }
};

export default { namespaced, state, getters, mutations, actions };
