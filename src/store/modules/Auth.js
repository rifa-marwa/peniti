const namespaced = true;

const state = {
  isLoading: false,
  token: localStorage.getItem('token') ?? null,
  refreshToken: localStorage.getItem('refreshToken') ?? null,
  user: {},
  authDataForm: {
    email: '',
    password: ''
  },
  authDataResetPasswordForm: {
    newPassword: '',
    passwordConfirmation: ''
  }
};

const getters = {
  isAuthenticated: state => !!state.token,
  token: state => state.token,
  refreshToken: state => state.refreshToken,
  user: state => state.user
};

const mutations = {
  SET_AUTH_LOADING: state => (state.isLoading = true),
  // TODO(Huda): Remove this when u no longer need to manually make isLoading false, XD.
  SET_AUTH_LOADING_END: state => (state.isLoading = false),
  SET_AUTH_DATA_FORM: (state, { authMode, target }) => {
    // Set auth data for login
    if (authMode === 'login') {
      state.authDataForm = {
        ...state.authDataForm,
        [target.name]: target.value
      };
    }

    // Set auth data for resetting password
    if (authMode === 'resetPassword') {
      state.authDataResetPasswordForm = {
        ...state.authDataResetPasswordForm,
        [target.name]: target.value
      };
    }
  },
  CLEAR_AUTH_DATA_FORM: state => {
    state.authDataForm = {
      ...state.authDataForm,
      email: '',
      password: ''
    };
    state.authDataResetPasswordForm = {
      ...state.authDataResetPasswordForm,
      newPassword: '',
      passwordConfirmation: ''
    };
  },
  SET_USER: (state, payload) => {
    state.user = payload;
    state.isLoading = false;
  },
  SET_AUTH: (state, { token, refreshToken, user }) => {
    state.token = token;
    state.refreshToken = refreshToken;
    state.user = user;

    localStorage.setItem('token', token);
    localStorage.setItem('refreshToken', refreshToken);
    state.isLoading = false;
  },
  LOGOUT: state => {
    state.token = null;
    state.refreshToken = null;
    state.user = {};
    state.authDataForm = {
      email: '',
      password: ''
    };

    localStorage.removeItem('token');
    localStorage.removeItem('refreshToken');
    state.isLoading = false;
  }
};

const actions = {
  getProfile: async ({ commit }) => {
    commit('SET_AUTH_LOADING');

    try {
      const {
        data: { results }
      } = await window.axios.get('/auth');

      commit('SET_USER', results);

      return Promise.resolve(true);
    } catch (err) {
      return Promise.reject(err);
    }
  },

  login: async ({ commit }, payload) => {
    commit('SET_AUTH_LOADING');

    try {
      const {
        data: {
          results: { token, refreshToken, user }
        }
      } = await window.axios.post('/auth', payload);

      commit('SET_AUTH', {
        token,
        refreshToken,
        user
      });
      commit('CLEAR_AUTH_DATA_FORM');

      return Promise.resolve(true);
    } catch (err) {
      return Promise.reject(err);
    }
  },

  logout: async ({ commit }, payload) => {
    commit('SET_AUTH_LOADING');

    try {
      // TODO(Huda): store to variable this await
      await window.axios.post('/auth/logout', payload);

      commit('LOGOUT');

      return Promise.resolve(true);
    } catch (err) {
      return Promise.reject(err);
    }
  },

  resetPassword: async ({ commit }, payload) => {
    commit('SET_AUTH_LOADING');

    try {
      // TODO(Huda): store to variable this await
      await window.axios.put('/auth/reset-password', payload);

      // TODO(Huda): Will be commited some mutations
      commit('SET_AUTH_LOADING_END');

      commit('CLEAR_AUTH_DATA_FORM');

      return Promise.resolve(true);
    } catch (err) {
      return Promise.reject(err);
    }
  }
};

export default { namespaced, state, getters, mutations, actions };
