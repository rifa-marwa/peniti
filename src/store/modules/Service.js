const namespaced = true;

const state = {
  isServiceLoading: false,
  serviceChartData: []
};

const getters = {};

const mutations = {
  SET_SERVICE_LOADING: state => (state.isServiceLoading = true),
  SET_SERVICE_CHART_DATA: (state, payload) => {
    state.serviceChartData = payload;
    state.isServiceLoading = false;
  }
};

const actions = {
  getServiceChartData: async ({ commit }) => {
    commit('SET_SERVICE_LOADING');

    try {
      const { data } = await window.axios.get('/services/chart-dashboard');

      let servicesChartValues = [];
      data.results.forEach(service => {
        servicesChartValues.push(service.value);
      });

      commit('SET_SERVICE_CHART_DATA', servicesChartValues);

      return Promise.resolve(servicesChartValues);
    } catch (err) {
      return Promise.reject(err);
    }
  }
};

export default { namespaced, state, getters, mutations, actions };
