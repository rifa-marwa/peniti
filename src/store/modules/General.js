const namespaced = true;

const state = {
  isSidebarOpen: true
};

const getters = {};

const mutations = {
  HANDLE_SIDEBAR: state => {
    state.isSidebarOpen = !state.isSidebarOpen;
  }
};

const actions = {};

export default { namespaced, state, getters, mutations, actions };
