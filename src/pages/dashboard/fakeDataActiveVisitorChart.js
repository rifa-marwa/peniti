export default {
  series: [
    // Filled with state
  ],
  chartOptions: {
    chart: {
      height: 260,
      type: 'bar',
      toolbar: {
        show: false
      },
      events: {
        // click: function(chart, w, e) {
        //   // console.log(chart, w, e)
        // }
      },
      fontFamily: 'Poppins-Regular'
    },
    plotOptions: {
      bar: {
        columnWidth: '68px',
        distributed: true
        // endingShape: 'rounded'
      }
    },
    dataLabels: {
      enabled: false
    },
    legend: {
      show: false
    },
    xaxis: {
      categories: [['Jan'], ['Feb'], ['Mar'], ['Apr'], ['May'], ['Jun'], ['Jul'], ['Aug'], ['Sep'], ['Oct'], ['Nov'], ['Dec']],
      labels: {
        style: {
          fontSize: '12px'
        }
      },
      axisBorder: {
        show: false
      },
      axisTicks: {
        show: false
      }
    }
  }
};
