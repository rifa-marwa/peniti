export default {
  series: [
    // Filled with state
  ],
  chartOptions: {
    chart: {
      width: '400px',
      type: 'donut'
    },
    labels: ['Starter', 'Essential', 'Growth', 'Advanced'],
    dataLabels: {
      enabled: true,
      style: {
        fontSize: '12px',
        fontFamily: 'Poppins-Regular',
        fontWeight: 'bold'
      }
    },
    legend: {
      show: true,
      position: 'bottom',
      fontFamily: 'Poppins-Regular'
    },
    responsive: [
      {
        breakpoint: 480,
        options: {
          chart: {
            width: 200
          },
          legend: {
            position: 'bottom'
          }
        }
      }
    ],
    plotOptions: {
      pie: {
        donut: {
          size: '65%',
          labels: {
            show: true,
            name: {
              show: true,
              fontSize: '22px',
              fontFamily: 'Poppins-Regular',
              fontWeight: 600,
              color: '#121212',
              offsetY: 18,
              formatter: function(val) {
                return val;
              }
            },
            value: {
              show: true,
              fontSize: '16px',
              fontFamily: 'Poppins-Regular',
              fontWeight: 600,
              color: '#121212',
              offsetY: -18,
              formatter: function(val) {
                return val;
              }
            },
            total: {
              show: true,
              showAlways: true,
              label: 'Total',
              fontSize: '15px',
              fontFamily: 'Poppins-Regular',
              fontWeight: 600,
              color: '#373d3f',
              formatter: function(w) {
                return w.globals.seriesTotals.reduce((a, b) => {
                  return a + b;
                }, 0);
              }
            }
          }
        }
      }
    }
  }
};
