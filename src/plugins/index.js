import './axios';
import './vuelidate';
import './vuelidate-error-extractor';
import './vuesax';
import './vue-moment';
import './vue-apex-chart';
