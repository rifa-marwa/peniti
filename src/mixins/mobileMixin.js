// Vuex
import { mapState } from 'vuex';

export default {
  data: () => ({
    windowWidth: 0,
    windowHeight: 0
  }),
  computed: {
    ...mapState('General', ['isSidebarOpen']),
    isAllMobileSizeWidth() {
      return this.windowWidth >= 0 && this.windowWidth <= 768;
    }
  },
  methods: {
    onResize(event) {
      if (event) {
        // Set window width and height to state
        const { innerWidth, innerHeight } = event.target ?? event;

        this.windowWidth = innerWidth;
        this.windowHeight = innerHeight;

        // Close sidebar
        if (innerWidth >= 0 && innerWidth <= 768 && this.isSidebarOpen) {
          this.$store.commit('General/HANDLE_SIDEBAR');
        }
      }
    }
  },
  mounted() {
    // Set initial value
    this.windowWidth = window.innerWidth;
    this.windowHeight = window.innerHeight;

    // Register an event listener when the Vue component is ready
    window.addEventListener('resize', event => {
      return this.onResize(event);
    });
  },
  beforeDestroy() {
    // Unregister the event listener before destroying this Vue instance
    window.removeEventListener('resize', this.onResize);
  }
};
