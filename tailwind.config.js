/*
 * --- configuration of tailwind ---
 */
module.exports = {
  future: {},
  purge: ['./public/**/*.html', './src/**/*.vue'],
  theme: {
    extend: {
      colors: {
        primary: '#3E89F9',
        secondary: '#c83492',
        success: '#00C703',
        warning: '#E2A230',
        danger: '#E50A0A',
        black: '#121212',
        white: '#ffffff',
        'grey-000': '#aaa',
        'grey-001': '#aaaaaa',
        'grey-002': '#dedede',
        'grey-003': '#ebedf8',
        'grey-004': '#f5f6fE'
      },
      fontFamily: {
        'poppins-regular': ['Poppins-Regular'],
        'poppins-bold': ['Poppins-Bold'],
        'poppins-semibold': ['Poppins-SemiBold']
      }
    }
  },
  variants: {},
  plugins: []
};
